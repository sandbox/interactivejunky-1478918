OVERVIEW
------------
A submodule for Privacy Per User (http://drupal.org/project/privacypu) that
allows testing of User Relationships
(http://drupal.org/project/user_relationships). The requestee of the
relationship is the current user and the requester is the user specified by the
argument passed to the Privacy Per User module. This module was created in
response to http://drupal.org/node/1240540.

FUNCTIONALITY
-------------
In the Privacy Per User module there are two settings; private and public, which
allow the user to set the visibility of their profile, nodes or other site
features on a Drupal site. This module integrates the User Relationships module
with Privacy Per User to allow the user to select user relationships as well as
private and public in their profile. For example a user could chose to share
their photographs or profile fields only with friends, similar to the way in
which Facebook works.

DRUPAL COMMONS
--------------
This module is currently being used on a production site and intranet using
Drupal Commons (which relies heavily on user relationships to provide follower
functionality).

INSTRUCTIONS
------------

* Select the user relationships available on the Privacy Per User settings
  page.
* All user relationships that the user has permission to request should now be
  available to them as a privacy state.

DEPENDENCIES
------------
* Privacy Per User (http://drupal.org/project/privacypu)
* User Relationships (http://drupal.org/project/user_relationships)

LIMITATIONS
------------
The module only allows testing of two-way relationships
and Requestee->requester one way relationships.

NOTES
-----
The user selecting the privacy state must have permission to access a user
relationship in order to select it.

CREDITS
------------
Created by Marton Bodonyi (http://www.interactivejunky.com)
and sponsored by Catch Digital (http://www.catchdigital.com).
